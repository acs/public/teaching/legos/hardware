# generates the diff image of the schematic (and the pcb with the same basename) given as first argument against the git revision given as second argument.
SCHEMATIC=$1
DIFF_SHA=$2

ls -l entities
ls -l entities/*

ENTITY=basename $SCHEMATIC .kicad_sch
PCB="${SCHEMATIC%.*}.kicad_pcb"

echo "ENTITY: $ENTITY"
echo "SCHEMATIC: $SCHEMATIC"
echo "PCB: $PCB"

set -e

# Generate schematic & pcb images
kicad-cli sch export svg $SCHEMATIC --output imgs
kicad-cli pcb export svg -l "F.Silkscreen,F.Cu,Edge.Cuts" $PCB --output imgs/${ENTITY}_pcb_front.svg
kicad-cli pcb export svg -l "B.Silkscreen,B.Cu,Edge.Cuts" -m $PCB --output imgs/${ENTITY}_pcb_back.svg

echo ====== comparing with $DIFF_SHA ======

# Checkout previous commit
git checkout $DIFF_SHA

mkdir imgs-pre

# Generate images for target branch commit
# the || true ensures the script continues on errors, as we can handle if there is no output
kicad-cli sch export svg $SCHEMATIC --output imgs-pre || true
kicad-cli pcb export svg -l "F.Silkscreen,F.Cu,Edge.Cuts" $PCB --output imgs-pre/${ENTITY}_pcb_front.svg || true
kicad-cli pcb export svg -l "B.Silkscreen,B.Cu,Edge.Cuts" -m $PCB --output imgs-pre/${ENTITY}_pcb_back.svg || true

mkdir -p diff
mkdir tmp
mkdir tmp/imgs
mkdir tmp/imgs-pre

echo "imgs"
ls -l imgs
echo "imgs pre:"
ls -l imgs-pre

generate_diff_image() {
    local file="$1"
    local basename=$(basename "${file}")

    echo ""
    echo "========= Generating diff image of $basename ========="

    inkscape -w 2048 "imgs/$basename" -o "tmp/imgs/${basename}.png"

    if [[ ! -e "imgs-pre/$basename" ]]; then
      echo "File is new, no diff necessary"
      mv "tmp/imgs/${basename}.png" "diff/${basename}_newfile.png"
      return
    fi

    inkscape -w 2048 "imgs-pre/$basename" -o "tmp/imgs-pre/${basename}.png"
    difference="$(compare "tmp/imgs/${basename}.png" "tmp/imgs-pre/${basename}.png" -metric AE -fuzz 1% null 2>&1)"

    echo "Difference between versions is ${difference}"
    if [ "${difference}" = "0" ] ; then
        echo "no change"
    else
        compare "tmp/imgs/${basename}.png" "tmp/imgs-pre/${basename}.png" -compose src -lowlight-color none "tmp/${basename}_diff.png"
        composite -dissolve 50 "tmp/${basename}_diff.png" "tmp/imgs/${basename}.png" "diff/${basename}_diff.png"
        echo "generated diff image $diff/${basename}_diff.png"
    fi
}

export -f generate_diff_image
find "imgs" -type f -print0 | parallel --null generate_diff_image {}
