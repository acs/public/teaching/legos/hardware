%% Regulation Master
% LT3081 as a Voltage Source
% AD8400ARZ10 for voltage regulation

Vdd = 5;        % Input voltage
Vdr = 1.22;     % Dropout voltage

Rw  = 50;   
Rwb = @(D,Rab)(D./256.*Rab+Rw);
Rwa = @(D,Rab)((256-D)./256.*Rab+Rw);
v   = @(D,Rab)((Rwb(D,Rab)+3e4)*2*50e-6);

D = 0:256;
Rab = [8;10;12]*1e3;
Vout= v(D,Rab);
Vout(Vout>(Vdd-Vdr)) = (Vdd-Vdr);
plot(D,Vout)
title('Voltage Regulation')
xlim([0 256])
xlabel('Code')
ylabel('Voltage (V)')
legend('Rab = 8 k\Omega','Rab = 10 k\Omega','Rab = 12 k\Omega','Location','Southeast')

%% AD8400ARZ1 for current limitation

Rw  = 50;
Rwb = @(D,Rab)(D./256.*Rab+Rw);
Rwa = @(D,Rab)((256-D)./256.*Rab+Rw);
i = @(D,Rab)((Rwb(D,Rab)+1.3e3-450/2)*0.36e-3*4);

D = 0:256;
Rab = [800;1200;1600];
plot(D,i(D,Rab))
title('Current Limitation')
xlim([0 256])
xlabel('Code')
ylabel('Current (A)')
legend('Rab = 0.8k','Rab = 1.2k','Rab = 1.6k','Location','Southeast')