# Power Plant
The Power Plant represents a traditional power producer from fossil fuel combustion. It fixes the voltage of the nominal voltage of the grid adopting a voltage source configuration, able to erogate up to 4 A when the output current limit is set to max. The current power load is visualized by the tower using a trichromatic color scale: green-yellow (0 % to 50 %) and yellow-red (50 % to 100 %). A slider on the side can be used for varying the output current limit. The circuitry includes a power generation sub-block, a telemetry sub-block for monitoring the power production in the 3.3 V bus and a sub-block for the visulization of the current output load.

## IO Pinout and Device Setup
### ESP32 Pinout
| IO Pin | Description |
| ------ | ----------- |
| IO00 | Node ID |
| IO17 | I2C SDA |
| IO05 | I2C SCL |
| IO04 | SPI MOSI |
| IO21 | SPI CLK |
| IO18 | Output Power <span style="text-decoration:overline">CS I</span> |
| IO19 | Output Power <span style="text-decoration:overline">CS V</span> |
| IO25 | PCA9956 <span style="text-decoration:overline">Output Enable</span> |
| IO16 | PCA9956 <span style="text-decoration:overline">Reset</span> |
| IO05 | <span style="text-decoration:overline">Sense Auto Presence</span> |
| IO17 | <span style="text-decoration:overline">Sense Heli Presence</span> |
| IO22 | Output Power Turbo Motor |
| IO26 | Output Power Cooling Fan |
| IO34 | Output Power Temperature |
| IO35 | Output Power Current Sense |
| IO13 | Touch Slider 1 |
| IO12 | Touch Slider 2 |
| IO14 | Touch Slider 3 |
| IO27 | Touch Slider 4 |
| IO32 | Touch Slider 5 |
| IO33 | Touch Slider 6 |

### INA209 Setup
| Parameter | Value |
| --------- | ----- |
| Address | 0x41 |
| Shunt | 0.02 Ohm |
| Max. Current | 8 A |
| PGA Gain | %4 |

### PCA9956 Setup
| Parameter | Value |
| --------- | ----- |
| Address | 0x19 |
| LED Min Current | 0.1125 mA |
| LED Min Current | 28.7 mA |

## Schematic
[<img src="docs/power_plant_sch.png"  width="936" height="500">](docs/power_plant_sch-main.png)

[<img src="docs/power_plant_sch-sub_connectors.png"  width="145" height="100">](docs/power_plant_sch-sub_connectors.png)
&nbsp;
[<img src="docs/power_plant_sch-sub_power_master.png"  width="145" height="100">](docs/power_plant_sch-sub_power_master.png)
&nbsp;
[<img src="docs/power_plant_sch-sub_power_auto.png"  width="145" height="100">](docs/power_plant_sch-sub_power_auto.png)
&nbsp;
[<img src="docs/power_plant_sch-sub_monitor.png"  width="145" height="100">](docs/power_plant_sch-sub_monitor.png)
&nbsp;
[<img src="docs/power_plant_sch-sub_telemetry.png"  width="145" height="100">](docs/power_plant_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/power_plant_sch-sub_mechanical.png"  width="145" height="100">](docs/power_plant_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/power_plant_pcb.png"  width="531" height="500">](docs/power_plant_pcb-brd.png)

[<img src="docs/power_plant_pcb-F_Cu.png"  width="145" height="100">](docs/power_plant_pcb-F_Cu.png)
&nbsp;
[<img src="docs/power_plant_pcb-In1_Cu.png"  width="145" height="100">](docs/power_plant_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/power_plant_pcb-In2_Cu.png"  width="145" height="100">](docs/power_plant_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/power_plant_pcb-B_Cu.png"  width="145" height="100">](docs/power_plant_pcb-B_Cu.png)

## Media

[<img src="docs/power_plant_3d-top.png"  width="121" height="100">](docs/power_plant_3d-top.png)
&nbsp;
[<img src="docs/power_plant_3d-bottom.png"  width="121" height="100">](docs/power_plant_3d-bottom.png)
&nbsp;
[<img src="docs/power_plant_brd.png"  width="185" height="100">](docs/power_plant_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/power_plant/power_plant.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/power_plant/power_plant.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/power_plant/power_plant.md)