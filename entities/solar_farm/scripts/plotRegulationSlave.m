%% Regulation Slave
% LT3081 as a Current Source
% AD8400ARZ1 for current regulation

Iset = 50e-6;
Rout = 0.5;

Rw  = 50;
Rwb = @(D,Rab)(D./256.*Rab+Rw);
Rwa = @(D,Rab)((256-D)./256.*Rab+Rw);
i = @(D,Rab)(Rwb(D,Rab)*Iset/Rout);

D = 0:256;
Rab = [8e3;10e3;12e3];
plot(D,i(D,Rab))
title('Current Regulation')
xlim([0 256])
xlabel('Code')
ylabel('Current (A)')
legend('Rab = 8k','Rab = 10k','Rab = 12k','Location','Southeast')