# Supply
This module is an adapter required to connect an external 5 V power supply to LEGOS using a branch port at any node.  The module protects the electrical layer from voltage spikes using a transient-voltage-suppression (TVS) diode, but attention should be paid when connecting the supply to the adapter, because the module **does NOT protect against reverse polarity**.

## Schematic
[<img src="docs/supply_sch.png"  width="1066" height="500">](docs/supply_sch-main.png)

## Printed Circuit Board

[<img src="docs/supply_pcb.png"  width="734" height="500">](docs/supply_pcb-brd.png)

[<img src="docs/supply_pcb-F_Cu.png"  width="145" height="100">](docs/supply_pcb-F_Cu.png)
&nbsp;
[<img src="docs/supply_pcb-In1_Cu.png"  width="145" height="100">](docs/supply_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/supply_pcb-In2_Cu.png"  width="145" height="100">](docs/supply_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/supply_pcb-B_Cu.png"  width="145" height="100">](docs/supply_pcb-B_Cu.png)

## Media

[<img src="docs/supply_3d_1port-top.png"  width="55" height="100">](docs/supply_3d_1port-top.png)
&nbsp;
[<img src="docs/supply_3d_1port-bottom.png"  width="58" height="100">](docs/supply_3d_1port-bottom.png)
&nbsp;
[<img src="docs/supply_brd.png"  width="95" height="100">](docs/supply_brd.png)
&nbsp;
[<img src="docs/supply_3d_2port-top.png"  width="72" height="100">](docs/supply_3d_2port-top.png)
&nbsp;
[<img src="docs/supply_3d_2port-bottom.png"  width="73" height="100">](docs/supply_3d_2port-bottom.png)


## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/supply/supply.md)
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/grid/supply/supply.md)