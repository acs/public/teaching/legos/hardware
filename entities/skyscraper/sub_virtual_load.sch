EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "Load Regulation"
Date "2020-05-31"
Rev "1.0"
Comp "EON ERC"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5ED4311C
P 5100 3300
F 0 "Q?" H 5304 3346 39  0000 L CNN
F 1 "2N7002NXA" H 5304 3255 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5300 3400 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/916/2N7002NXAK-1651343.pdf" H 5100 3300 50  0001 C CNN
	1    5100 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5ED43128
P 4300 2750
F 0 "#PWR?" H 4300 2600 50  0001 C CNN
F 1 "+5V" H 4315 2923 39  0000 C CNN
F 2 "" H 4300 2750 50  0001 C CNN
F 3 "" H 4300 2750 50  0001 C CNN
	1    4300 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5ED43134
P 5200 4050
F 0 "R?" H 5259 4096 39  0000 L CNN
F 1 "30 - 1/2W" H 5259 4005 39  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5200 4050 50  0001 C CNN
F 3 "~" H 5200 4050 50  0001 C CNN
F 4 "ERJ-P06F30R0V" H 5200 4050 50  0001 C CNN "Mfr"
	1    5200 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3400 4000 3400
Wire Wire Line
	4000 3400 4000 3850
$Comp
L power:GND #PWR?
U 1 1 5ED43154
P 3650 3500
F 0 "#PWR?" H 3650 3250 50  0001 C CNN
F 1 "GND" H 3655 3327 39  0000 C CNN
F 2 "" H 3650 3500 50  0001 C CNN
F 3 "" H 3650 3500 50  0001 C CNN
	1    3650 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3200 4100 3200
Text Notes 5400 4050 0    50   ~ 0
MAX Current: 100mA
Text Notes 3750 3150 0    50   ~ 0
0-3.3 V
Text HLabel 2950 3200 0    39   Input ~ 0
IN
Wire Wire Line
	4000 3850 5200 3850
Wire Wire Line
	4900 3300 4800 3300
$Comp
L Device:R_Small R?
U 1 1 5ED66060
P 4800 3500
F 0 "R?" H 4859 3546 39  0000 L CNN
F 1 "10k" H 4859 3455 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4800 3500 39  0001 C CNN
F 3 "~" H 4800 3500 39  0001 C CNN
	1    4800 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ED66CAF
P 4800 3600
F 0 "#PWR?" H 4800 3350 39  0001 C CNN
F 1 "GND" H 4805 3427 39  0000 C CNN
F 2 "" H 4800 3600 39  0001 C CNN
F 3 "" H 4800 3600 39  0001 C CNN
	1    4800 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3400 4800 3300
Connection ~ 4800 3300
Wire Wire Line
	4800 3300 4700 3300
$Comp
L Device:R_Small R?
U 1 1 5ED8EE68
P 3400 3200
F 0 "R?" V 3600 3150 39  0000 L CNN
F 1 "4.7k" V 3500 3150 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3400 3200 39  0001 C CNN
F 3 "~" H 3400 3200 39  0001 C CNN
	1    3400 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 3200 3650 3200
Connection ~ 3650 3200
Wire Wire Line
	2950 3200 3150 3200
$Comp
L Device:C_Small C?
U 1 1 5ED904F3
P 3650 3400
F 0 "C?" H 3600 3350 39  0000 R CNN
F 1 "100n" H 3650 3450 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3650 3400 39  0001 C CNN
F 3 "~" H 3650 3400 39  0001 C CNN
	1    3650 3400
	1    0    0    1   
$EndComp
Wire Wire Line
	3650 3300 3650 3200
Connection ~ 5200 3850
Wire Wire Line
	5200 3500 5200 3850
Wire Wire Line
	5200 3850 5200 3950
$Comp
L power:GND #PWR?
U 1 1 5EF4BC50
P 4500 2750
F 0 "#PWR?" H 4500 2500 39  0001 C CNN
F 1 "GND" H 4505 2577 39  0000 C CNN
F 2 "" H 4500 2750 39  0001 C CNN
F 3 "" H 4500 2750 39  0001 C CNN
	1    4500 2750
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF4A737
P 4500 2850
F 0 "C?" H 4450 2800 39  0000 R CNN
F 1 "100n" H 4500 2900 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 2850 39  0001 C CNN
F 3 "~" H 4500 2850 39  0001 C CNN
	1    4500 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4300 3000 4300 2950
Wire Wire Line
	4300 2950 4500 2950
Connection ~ 4300 2950
Wire Wire Line
	4300 2950 4300 2750
Wire Wire Line
	5200 2750 5200 3100
$Comp
L Device:R_Small R?
U 1 1 5F06630D
P 3150 3400
F 0 "R?" H 3209 3446 39  0000 L CNN
F 1 "100k" H 3209 3355 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3150 3400 39  0001 C CNN
F 3 "~" H 3150 3400 39  0001 C CNN
	1    3150 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F066313
P 3150 3500
F 0 "#PWR?" H 3150 3250 39  0001 C CNN
F 1 "GND" H 3155 3327 39  0000 C CNN
F 2 "" H 3150 3500 39  0001 C CNN
F 3 "" H 3150 3500 39  0001 C CNN
	1    3150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3300 3150 3200
Connection ~ 3150 3200
Wire Wire Line
	3150 3200 3300 3200
$Comp
L power:GND #PWR?
U 1 1 5F06152D
P 4300 3600
F 0 "#PWR?" H 4300 3350 50  0001 C CNN
F 1 "GND" H 4305 3427 39  0000 C CNN
F 2 "" H 4300 3600 50  0001 C CNN
F 3 "" H 4300 3600 50  0001 C CNN
	1    4300 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F062118
P 5200 4150
F 0 "#PWR?" H 5200 3900 39  0001 C CNN
F 1 "GND" H 5205 3977 39  0000 C CNN
F 2 "" H 5200 4150 39  0001 C CNN
F 3 "" H 5200 4150 39  0001 C CNN
	1    5200 4150
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:AD8603 U?
U 1 1 5F309BDD
P 4400 3300
F 0 "U?" H 4400 3550 39  0000 L CNN
F 1 "TLV333IDBV" H 4400 3450 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 4400 3300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlv333.pdf" H 4400 3500 50  0001 C CNN
	1    4400 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F3565D2
P 5200 2750
F 0 "#PWR?" H 5200 2600 50  0001 C CNN
F 1 "+3.3V" H 5215 2915 39  0000 C CNN
F 2 "" H 5200 2750 50  0001 C CNN
F 3 "" H 5200 2750 50  0001 C CNN
	1    5200 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
